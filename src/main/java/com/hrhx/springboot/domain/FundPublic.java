package com.hrhx.springboot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * 
 * @author duhongming
 *
 */
@Entity
public class FundPublic {
	
	@Id
    @GeneratedValue
    private Long id;
	
	@Column(nullable = false)
	private String fundCode;
	
	@Column(nullable = false)
	private String fundName;
	
	public FundPublic() {
		super();
	}

	public FundPublic(String fundCode, String fundName) {
		super();
		this.fundCode = fundCode;
		this.fundName = fundName;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

}
