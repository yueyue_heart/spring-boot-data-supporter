package com.hrhx.springboot.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hrhx.springboot.domain.Cnarea;
import com.hrhx.springboot.jopo.ResultBean;
import com.hrhx.springboot.mongodb.repository.CnareaRepository;
import com.hrhx.springboot.mysql.service.CnareaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * 
 * @author duhongming
 *
 */
@Api(value="全国省市区乡村五级联动数据",tags="js调用")
@RestController
@RequestMapping(value = "/cnarea")
public class CnareaRestController {

	@Autowired
	private CnareaService cnareaService;
	
	@Autowired
	private CnareaRepository cnareaRepository;
	
	/**
	 * 当Mysql请求超时的时候，能够以最快的速度切换到MongoDB
	 */
	private static Boolean isMysqlConnection = true;

	@ApiOperation(value = "获取省级详细信息", notes = "/cnarea/list")
	@ApiResponses({
        @ApiResponse(code=400,message="请求参数没填好"),
        @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
    })
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResultBean<List<Cnarea>> getAllFirstLevel() {
		List<Cnarea> cnareaList = null;
		if(!isMysqlConnection){
			return new ResultBean<List<Cnarea>>(cnareaRepository.findByLevel(0));
		}
		try{//默认先执行Mysql
			isMysqlConnection = true;
			cnareaList = cnareaService.getAllFirstLevel();
			System.out.println("Mysql：获取省级详细信息");
		}catch(Exception e){//出现异常用MongoDB	
			isMysqlConnection = false;
			cnareaList = cnareaRepository.findByLevel(0);
			System.out.println("MongoDB：获取省级详细信息");
		}
		return new ResultBean<List<Cnarea>>(cnareaList);
	}

	@ApiOperation(value = "获取level级以及关联上级的详细信息", notes = "/cnarea/query?id=1&level=1")
	@ApiImplicitParams({
		
        @ApiImplicitParam(name = "cnarea", value = "地理详细信息", required = false, dataType = "Cnarea")
	})
	@ApiResponses({
        @ApiResponse(code=400,message="请求参数没填好"),
        @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
    })
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public ResultBean<List<Cnarea>> getByParentId(
			@ApiParam(required = true, name = "level", value= "省市区乡村分别代表1,2,3,4,5", defaultValue="1" ) 
			@RequestParam( value = "level") Integer level,
			@ApiParam(required = true, name = "id", value= "该level的id", defaultValue="1" ) 
			@RequestParam( value = "id") Integer id) {
		List<Cnarea> cnareaList = null;	
		if(!isMysqlConnection){
			return new ResultBean<List<Cnarea>>(cnareaRepository.findByLevelAndParentId(level,id));
		}
		try{//默认先执行Mysql
			isMysqlConnection = true;
			cnareaList = cnareaService.getByParentId(level,id);
			System.out.println("Mysql：获取level级以及关联上级的详细信息");
		}catch(Exception e){//出现异常用MongoDB	
			isMysqlConnection = false;
			cnareaList = cnareaRepository.findByLevelAndParentId(level,id);
			System.out.println("MongoDB：获取level级以及关联上级的详细信息");
		}
		System.out.println("获取Level级别："+id);
		System.out.println("获取上级Id："+level);
		return new ResultBean<List<Cnarea>>(cnareaList);
	}

}